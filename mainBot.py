from botCreate import dp
from aiogram.utils import executor

async def onStartup(_):
    print('bot started')

from botModules import botClient, botUtils, botAdmin

botAdmin.registerHandlersAdmin(dp)
botClient.registerHandlersClient(dp)


executor.start_polling(dp, skip_updates=True, on_startup=onStartup)
