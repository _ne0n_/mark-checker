from aiogram import types, Dispatcher
from botCreate import bot
from aiogram.types import ReplyKeyboardMarkup, KeyboardButton
import logging
from modules.repo import DBWork
# from modules.registrste import registration
from botModules.botClient import mainMenuAdmin, banned_users
from timerCheck import main

logging.basicConfig(level=logging.INFO, filename='.markChecker.log', format='%(asctime)s %(name)s %(levelname)s:%(message)s')
logger = logging.getLogger(__name__)

btnNames = KeyboardButton('/names')
btnMenu = KeyboardButton('/main')
btnAllTG = KeyboardButton('/ban')
btnCheckForAll = KeyboardButton('/ALLCHECK')
admins = (878903630)

adminMenu = ReplyKeyboardMarkup(resize_keyboard = True).add(btnNames, btnMenu, btnAllTG, btnCheckForAll)

async def ban(msg : types.message):
    if not await checkPerm(msg):
        return
    id = msg.text.split(' ')[1]
    banned_users.add(id)

async def initAdmin(msg : types.message):
    if not await checkPerm(msg):
        return
    await bot.send_message(msg.from_user.id, 'Инициализация админ доступа...', reply_markup=adminMenu)

async def menu(msg : types.message):
    if not await checkPerm(msg):
        return
    await bot.send_message(msg.from_user.id, 'Переход в главное меню...', reply_markup=mainMenuAdmin)

async def allNames(msg : types.message):
    if not await checkPerm(msg):
        return
    ans = ''
    allNames = DBWork.getAllNames()
    for i in allNames:
        ans += i[0] + '\n'
    await bot.send_message(msg.from_user.id, ans)

async def checkForAll(msg : types.message):
    if not await checkPerm(msg):
        return
    await main()

async def checkPerm(msg):
    if msg.from_user.id not in admins:
        await bot.send_message(msg.from_user.id, 'Нет прав администратора')
        return False
    return True

def registerHandlersAdmin(dp : Dispatcher):
    dp.register_message_handler(initAdmin, commands=['admin'])
    dp.register_message_handler(menu, commands=['main'])
    dp.register_message_handler(allNames, commands=['names'])
    dp.register_message_handler(checkForAll, commands=['ALLCHECK'])