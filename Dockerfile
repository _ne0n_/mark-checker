FROM python:3

WORKDIR /usr/src/app

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY botModules botModules
COPY modules modules
COPY netschoolapi netschoolapi
COPY mainBot.py mainBot.py
COPY botCreate.py botCreate.py
COPY timerCheck.py timerCheck.py
COPY migrationDB.py migrationDB.py

CMD ["python", "./mainBot.py"]
