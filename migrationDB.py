import sqlite3

database = sqlite3.connect('data.db', timeout=20)

database = sqlite3.connect('data.db')
database.cursor().execute('CREATE TABLE "marks" (\
	"id"	INTEGER,\
	"student_id"	INTEGER,\
	"subj"	TEXT,\
	"mark"	TEXT,\
	"type"	TEXT,\
	"date"	TEXT,\
	PRIMARY KEY("id" AUTOINCREMENT)\
);')
database.cursor().execute('CREATE TABLE "students" (\
	"id"	INTEGER,\
	"user_id"	INTEGER,\
	"external_id"	INTEGER,\
	"name"	TEXT,\
	PRIMARY KEY("id" AUTOINCREMENT)\
);')
database.cursor().execute('CREATE TABLE "telegram" (\
	"id"	INTEGER,\
	"user_id"	INTEGER,\
	"tg_id"	TEXT,\
	PRIMARY KEY("id" AUTOINCREMENT)\
);')
database.cursor().execute('CREATE TABLE "users" (\
	"id"	INTEGER,\
	"login"	TEXT NOT NULL UNIQUE,\
	"pass"	TEXT NOT NULL,\
	PRIMARY KEY("id" AUTOINCREMENT)\
);')
database.commit()
print('created')
database.close()