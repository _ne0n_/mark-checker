import modules
from modules.repo import DBWork
# from modules.client import clientWork
import datetime
import asyncio
import logging
from botModules import botClient
from datetime import date, timedelta

logging.basicConfig(level=logging.INFO, filename='.markChecker.log', format='%(asctime)s %(name)s %(levelname)s:%(message)s')
logger = logging.getLogger(__name__)

class registration:
    async def check(login : str, password : str, tg_id : int):
        if not DBWork.getUserId(login):
            print('[*]no user')
            ifExist = await modules.clientWork.checkSGOUser(login, password, tg_id)
            if not ifExist:
                return False
            else:
                return True
        else:
            print('[*]already exist')
            logger.error("trying registrate existing user %s" % (login))
            return False
    
    async def initNewUser(login : str, password : str, tg_id):
        # try:
        if await registration.check(login, password, tg_id):
            await botClient.sendCustomMsg(tg_id, 'Регистрация займет некоторое время...')
            students = await modules.clientWork.getAllChild(login, password, 'МАОУ лицей №64')
            DBWork.addUser(login, password, students, tg_id)
            if datetime.datetime.now().month == 9 or datetime.datetime.now().month == 10 or datetime.datetime.now().month == 11 or datetime.datetime.now().month == 12:
                for i in range (0, len(students)):
                    data = await modules.clientWork.getDiary(login, password, 'МАОУ лицей №64', students[i]['id'], datetime.date(datetime.datetime.now().year, 9, 1), date.today())
                    for j in range (0, len(data)):
                        try:
                            if DBWork.checkMark(DBWork.getStudentIDByEXT(students[i]['id']), data[j]['type'], data[j]['mark'], data[j]['subject'], data[j]['date']):
                                    DBWork.uploadMark(DBWork.getStudentIDByEXT(students[i]['id']), data[j]['type'], data[j]['mark'], data[j]['subject'], data[j]['date'])
                        except:
                            DBWork.deliteUser(login, students)
                            logger.error('network err')
                            await botClient.sendCustomMsg(tg_id, 'Ошибка при регистрации')
            else:
                for i in range (0, len(students)):
                    data = await modules.clientWork.getDiary(login, password, 'МАОУ лицей №64', students[i]['id'], datetime.date(datetime.datetime.now().year, 1, 1), date.today())
                    for j in range (0, len(data)):
                        try:
                            if DBWork.checkMark(DBWork.getStudentIDByEXT(students[i]['id']), data[j]['type'], data[j]['mark'], data[j]['subject'], data[j]['date']):
                                DBWork.uploadMark(DBWork.getStudentIDByEXT(students[i]['id']), data[j]['type'], data[j]['mark'], data[j]['subject'], data[j]['date'])
                        except:
                            DBWork.deliteUser(login, students)
                            logger.error('network err')
                            await botClient.sendCustomMsg(tg_id, 'Ошибка при регистрации')
                logger.info("new user %s created" % (login))
                await registration.checkAllMarks(DBWork.getNameByLogin(login), tg_id)
                await botClient.sendCustomMsg(tg_id, 'Вы успешно зарегистрированы!')
            
        else:
            await botClient.sendCustomMsg(tg_id, 'Вы ужe были зарегистрированы')
        # except:
        #     logger.error('network err')
        #     await botClient.sendCustomMsg(tg_id, 'Ошибка при регистрации')

    async def checkForOne(name, tg_id):
        ifMark = False
        if DBWork.getStudentByName(name):
            login = DBWork.getStudentByName(name)[0]
            password = DBWork.getStudentByName(name)[1]
            students = await modules.clientWork.getOneChild(login, password, name, 'МАОУ лицей №64')
            for i in range (0, len(students)):
                print('Checking for %s' % (students[i]['name']))
                await botClient.sendCustomMsg(tg_id, 'Проверяю для пользователя %s' % (students[i]['name']))
                end = date.today()
                start = date.today() - timedelta(days=14)
                data = await modules.clientWork.getDiary(login, password, 'МАОУ лицей №64', students[i]['id'], start, end)
                for j in range (0, len(data)):
                    if DBWork.checkMark(DBWork.getStudentIDByEXT(students[i]['id']), data[j]['type'], data[j]['mark'], data[j]['subject'], data[j]['date']):
                        for k in DBWork.getTgByName(name):
                            await botClient.sendCustomMsg(k[0], '%s получил оценку %s по предмету %s за число %s' % (students[i]['name'], data[j]['mark'], data[j]['subject'], str(data[j]['date'])))
                        DBWork.uploadMark(DBWork.getStudentIDByEXT(students[i]['id']), data[j]['type'], data[j]['mark'], data[j]['subject'], data[j]['date'])
                        ifMark = True
            if not ifMark:
                await botClient.sendCustomMsg(tg_id, 'Нет новых оценок')

    async def checkAllMarks(name, tg_id):
        msg = 'ВСЕ ОЦЕНКИ\n'
        subj = DBWork.getSubjList(name)
        print(name)
        print(subj)
        for i in subj:
            line = ''
            line += i + ': '
            marks = DBWork.getAllMarksBySubj(name, i)
            for j in marks:
                    line += j + ' '
            msg += line + '\n'
        await botClient.sendCustomMsg(tg_id, msg)

    async def ifAlotCheck(tg_id):
        if DBWork.getCountUser(tg_id):
            await registration.checkAllMarks(DBWork.getNameByTg(tg_id), tg_id)
        else:
            await botClient.sendChooseMsgList(tg_id, DBWork.getStudentsByTg(tg_id))

    async def ifAlotUsers(tg_id):
        if DBWork.getCountUser(tg_id):
            await registration.checkForOne(DBWork.getNameByTg(tg_id), tg_id)
        else:
            await botClient.sendChooseMsg(tg_id, DBWork.getStudentsByTg(tg_id))

    async def addNewTgIdToUser(login, password, tg_id):
        if not await registration.check(login, password, tg_id):
            DBWork.addTgToUser(tg_id, login)
            await botClient.sendCustomMsg(tg_id, 'Новый телеграм подключен')
        else:
            print('not exist')
            await botClient.sendCustomMsg(tg_id, 'Сначала зарегистрируйтесь')


if __name__ == '__main__':
    # botClient.sendCustomMsg(878903630, 'Тестовая отправка')
    print(asyncio.run(modules.clientWork.getAllChild('ДКравцова', 'ivanivan', 'МАОУ лицей №64')))