import asyncio
import time
import logging
from botModules import botClient
from modules.repo import DBWork
from netschoolapi.netschoolapi import netschoolapi
# from datetime import date, timedelta

logging.basicConfig(level=logging.INFO, filename='.markChecker.log', format='%(asctime)s %(name)s %(levelname)s:%(message)s')
logger = logging.getLogger(__name__)

class clientWork:
    async def getAllChild(login : str, password : str, school : str = 'МАОУ лицей №64'):
        try:
            ns = netschoolapi.NetSchoolAPI('https://sgo.rso23.ru/')
            students = []
            try:
                await ns.login(
                    login,
                    password,        
                    school,           
                    # 'МАОУ лицей №64',   
                )
            except:
                time.sleep(2)
                await ns.login(
                    login,
                    password,        
                    school,           
                    # 'МАОУ лицей №64',
                    )
            
                   
            for i in range(0, len(ns._students)):  
                student = {}
                student['name'] = ns._students[i]['nickName']
                student['id'] = ns._students[i]['studentId']
                student['elem'] = i
                students.append(student)
            
            # ns._student_id = ns._students[2]
            
            await ns.logout()
            return students
        except:
            await botClient.sendCustomMsg(DBWork.getTgByUser(login), 'Ошибка входа')
            logging.error('GetStudents err')


    async def getOneChild(login : str, password : str,  name : str, school : str = 'МАОУ лицей №64'):
        try:
            ns = netschoolapi.NetSchoolAPI('https://sgo.rso23.ru/')
            students = []
            try:
                await ns.login(
                    login,
                    password,        
                    school,           
                    # 'МАОУ лицей №64',   
                )
            except:
                time.sleep(2)
                await ns.login(
                    login,
                    password,        
                    school,           
                    # 'МАОУ лицей №64',
                    )
            
                   
            for i in range(0, len(ns._students)):
                if ns._students[i]['nickName'] == name:
                    student = {}
                    student['name'] = ns._students[i]['nickName']
                    student['id'] = ns._students[i]['studentId']
                    student['elem'] = i
                    students.append(student)
            
            # ns._student_id = ns._students[2]
            
            await ns.logout()
            return students
        except:
            await botClient.sendCustomMsg(DBWork.getTgByUser(login), 'Ошибка входа')
            logging.error('GetStudents err')


    async def checkSGOUser(login : str, password : str, tg_id : int, school : str = 'МАОУ лицей №64'):
        try:
            ns = netschoolapi.NetSchoolAPI('https://sgo.rso23.ru/')
            await ns.login(
            login,
            password,        
            school,           
            # 'МАОУ лицей №64',   
            )

            await ns.logout()
            return True
        except:
            await botClient.sendCustomMsg(tg_id, 'Неверный логин или пароль')
            logging.error("incorrect pass or user while registrate %s with pass %s" % (login, password))
            return False
        
    async def getDiary(login : str, password : str, school : str, studentID, start, end):
        # getAPI
        # try:
            start_time = time.time()
            ns = netschoolapi.NetSchoolAPI('https://sgo.rso23.ru/')

            # login
            await ns.login(
                login,
                password,        
                school,           
                # 'МАОУ лицей №64',   
            )

            # get diary
            diary = await ns.diary(studentID, start, end)

            # compile data

            data = []
            
            for i in range (0, len(diary.schedule)):
                for j in range (0, len(diary.schedule[i].lessons)):
                    for k in range (0, len(diary.schedule[i].lessons[j].assignments)):
                            if diary.schedule[i].lessons[j].assignments[k].mark != None:
                                lesson = {'subject' : 'None', 'date' : 'None', 'mark' : '', 'type' : ''}
                                lesson['subject'] = str(diary.schedule[i].lessons[j].subject)
                                lesson['date'] = str(diary.schedule[i].day)
                                lesson['mark'] = str(diary.schedule[i].lessons[j].assignments[k].mark)
                                lesson['type'] = str(diary.schedule[i].lessons[j].assignments[k].type)
                                data.append(lesson)

            # logOut
            await ns.logout()

            # return

            logger.info("--- %s seconds --- spent on user %s" % (time.time() - start_time, login))
            return data
        # except:
        #     await botClient.sendCustomMsg(DBWork.getTgByUser(login), 'Ошибка подключения к журналу')
        #     logging.error('connecktion err')

if __name__ == '__main__':
    print(asyncio.run(clientWork.getAllChild('ВПисарева', '288288', 'МАОУ лицей №64')))