from aiogram import types, Dispatcher
from botCreate import dp, bot
from aiogram.types import ReplyKeyboardMarkup, KeyboardButton
import logging
from modules.repo import DBWork
from modules.registrste import registration
from aiogram.dispatcher.filters import Text

logging.basicConfig(level=logging.INFO, filename='.markChecker.log', format='%(asctime)s %(name)s %(levelname)s:%(message)s')
logger = logging.getLogger(__name__)

btnReg = KeyboardButton('/reg')
btnHelp = KeyboardButton('/help')
btnCheck = KeyboardButton('/check')
btnLogin = KeyboardButton('/login')
btnAll = KeyboardButton('/all_marks')
btnAdminBar = KeyboardButton('/admin')

mainMenu = ReplyKeyboardMarkup(resize_keyboard = True).add(btnHelp, btnReg, btnLogin, btnCheck, btnAll)
mainMenuAdmin = ReplyKeyboardMarkup(resize_keyboard = True).add(btnHelp, btnReg, btnLogin, btnCheck, btnAll, btnAdminBar)
banned_users = (101650302)
admins = (878903630)

@dp.message_handler(user_id=banned_users)
async def handle_banned(msg: types.message):
    print(f"{msg.from_user.full_name} пишет, но мы ему не ответим!")
    return True


async def reg(msg : types.message):
    input:str = msg.text
    if len(input.split(' ')) != 3:
        await bot.send_message(msg.from_user.id, "Данные введены некорректно")
    else:
        login = input.split(' ')[1]
        word = input.split(' ')[2]
    await registration.initNewUser(login, word, msg.from_user.id)



async def help(msg : types.message):
    menu = mainMenu
    if msg.from_user.id in admins:
        menu = mainMenuAdmin
    await bot.send_message(msg.from_user.id, 'Список команд: \
        \n/start - показать приветственное сообщение \
        \n/help - показать приветственное сообщение \
        \n/reg ВашЛогин ВашПароль - зарегистрироваться в системе бота \
        \n/login ВашЛогин ВашПароль - привязать этот телеграм аккаунт к системе бота(если уже зарегистрированы с другого телеграм аккаунта) \
        \n/check - проверить наличие новых оценок за последние две недели \
        \n/all_marks - показать все оценки', reply_markup = menu)
    logger.info('sent help msg for %s' % (msg.from_user.id))

async def check(msg : types.message):
    await registration.ifAlotUsers(msg.from_user.id)


async def login(msg : types.message):
    if len(input.split(' ')) != 3:
        await bot.send_message(msg.from_user.id, "Данные введены некорректно")
    else:
        login = input.split(' ')[1]
        word = input.split(' ')[2]
        if not DBWork.getUserByTg(msg.from_user.id, login):
            await registration.addNewTgIdToUser(login, word, msg.from_user.id)
        else:
            await sendCustomMsg(msg.from_user.id, 'Этот телеграм уже привязан или пользователь не зарегистрирован')
    
async def sendCustomMsg(tg_id : int, msg : str):
    await bot.send_message(tg_id, msg)

async def sendChooseMsg(tg_id, names):
    keyboard = types.InlineKeyboardMarkup()
    for i in range(len(names)):
        keyboard.add(types.InlineKeyboardButton(text=names[i]['name'][0], callback_data="initCheck_%s" % (names[i]['name'])))
    await bot.send_message(tg_id, "Выберите ученика", reply_markup=keyboard)

async def sendChooseMsgList(tg_id, names):
    keyboard = types.InlineKeyboardMarkup()
    for i in range(len(names)):
        keyboard.add(types.InlineKeyboardButton(text=names[i]['name'][0], callback_data="initAll_%s" % (names[i]['name'])))
    await bot.send_message(tg_id, "Выбирите ученика", reply_markup=keyboard)

async def checkAll(msg : types.message):
    await registration.ifAlotCheck(msg.from_user.id)


@dp.callback_query_handler(Text(startswith="initCheck_"))
async def chekMenu(call: types.CallbackQuery):
    action = call.data.split("_")[1]
    await registration.checkForOne(action, call.from_user.id)
    await call.answer()

@dp.callback_query_handler(Text(startswith="initAll_"))
async def chekAll(call: types.CallbackQuery):
    action = call.data.split("_")[1]
    await registration.checkAllMarks(action, call.from_user.id)
    await call.answer()


def registerHandlersClient(dp : Dispatcher):
    dp.register_message_handler(login, commands=['login'])
    dp.register_message_handler(check, commands=['check'])
    dp.register_message_handler(checkAll, commands=['all_marks'])
    dp.register_message_handler(reg, commands=['reg'])
    dp.register_message_handler(help, commands=['help', 'start'])
