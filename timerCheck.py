import modules
from modules.repo import DBWork
from botModules import botClient
import asyncio
from datetime import date, timedelta


async def main():
    print('init timer check')
    allNames = DBWork.getAllNames()
    for i in allNames:
        await timerCheck(i[0])

async def timerCheck(name):
        if DBWork.getStudentByName(name):
            login = DBWork.getStudentByName(name)[0]
            password = DBWork.getStudentByName(name)[1]
            students = await modules.clientWork.getOneChild(login, password, name, 'МАОУ лицей №64')
            for i in range (0, len(students)):
                print('Checking for %s' % (students[i]['name']))
                # print(DBWork.getTgByName(name), 'Проверяю для пользователя %s' % (students[i]['name']))
                end = date.today()
                start = date.today() - timedelta(days=14)
                data = await modules.clientWork.getDiary(login, password, 'МАОУ лицей №64', students[i]['id'], start, end)
                for j in range (0, len(data)):
                    if DBWork.checkMark(DBWork.getStudentIDByEXT(students[i]['id']), data[j]['type'], data[j]['mark'], data[j]['subject'], data[j]['date']):
                        for k in DBWork.getTgByName(name):
                            await botClient.sendCustomMsg(k[0], '%s получил оценку %s по предмету %s за число %s' % (students[i]['name'], data[j]['mark'], data[j]['subject'], str(data[j]['date'])))
                        DBWork.uploadMark(DBWork.getStudentIDByEXT(students[i]['id']), data[j]['type'], data[j]['mark'], data[j]['subject'], data[j]['date'])

if __name__ == '__main__':
    asyncio.run(main())

