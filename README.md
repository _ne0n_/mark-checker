# MarksCheck

1. Первый запуск (и каждый раз когда меняется код):
   docker-compose up -d --build

2. Запуск:
   docker-compose up -d

3. Остановка:
   docker-compose down
