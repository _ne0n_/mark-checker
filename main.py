import asyncio
import logging
from datetime import date, timedelta
from botModules import botClient
from modules.client import clientWork
from modules.repo import DBWork
from modules.registrste import registration
from botModules.botClient import sendCustomMsg

logging.basicConfig(level=logging.INFO, filename='.markChecker.log', format='%(asctime)s %(name)s %(levelname)s:%(message)s')
logger = logging.getLogger(__name__)

async def main():

    for j in range (0, len(DBWork.getAllId())):
        students = await clientWork.getAllChild(DBWork.getUser(j + 1)[0], DBWork.getUser(j + 1)[1], 'МАОУ лицей №64')
        for i in range (0, len(students)):
            print(f'[*]user {j}, student {i}')
            end = date.today()
            start = date.today() - timedelta(days=14)
            data = await clientWork.getDiary(DBWork.getUser(j + 1)[0], DBWork.getUser(j + 1)[1], 'МАОУ лицей №64', students[i]['id'], start, end)
            for k in range (0, len(data)):
                if DBWork.checkMark(DBWork.getStudentIDByEXT(students[i]['id']), data[k]['type'], data[k]['mark'], data[k]['subject'], data[k]['date']):
                    DBWork.uploadMark(DBWork.getStudentIDByEXT(students[i]['id']), data[k]['type'], data[k]['mark'], data[k]['subject'], data[k]['date'])
                    await botClient.sendCustomMsg(DBWork.getTgByUser(DBWork.getUser(j + 1)[0]), '%s получил оценку %s по предмету %s за число %s' % (students[i]['name'], data[j]['mark'], data[j]['subject'], str(data[j]['date'])))

    logger.info("all marks checked")

async def newUser(login, password):
    await registration.initNewUser(login, password)

if __name__ == '__main__':
    asyncio.run(main())