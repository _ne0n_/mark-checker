import sqlite3
import logging


logging.basicConfig(level=logging.INFO, filename='.markChecker.log', format='%(asctime)s %(name)s %(levelname)s:%(message)s')
logger = logging.getLogger(__name__)

database = sqlite3.connect('data.db', timeout=20)

class DBWork:
    def getAllId():
        database = sqlite3.connect('data.db')
        cursor = database.cursor()
        cursor.execute("SELECT id FROM users")
        allId = cursor.fetchall()
        database.close()
        return allId
    
    def getAllNames():
        database = sqlite3.connect('data.db')
        cursor = database.cursor()
        cursor.execute("SELECT name FROM students")
        allNames = cursor.fetchall()
        database.close()
        return allNames

    def getUser(id : str):
        database = sqlite3.connect('data.db')
        cursor = database.cursor()
        cursor.execute("SELECT * FROM users WHERE id = '%s'" % (id))
        data = cursor.fetchall()
        login = data[0][1]
        password = data[0][2]
        database.close()
        return login, password

    def getTgByUser(login):
        database = sqlite3.connect('data.db')
        cursor = database.cursor()
        cursor = cursor.execute('SELECT tg_id FROM telegram WHERE user_id = %d' % (DBWork.getUserId(login)))
        id = cursor.fetchone()
        if id != None:
            id = id[0]
            database.close()
            return id
        else:
            database.close()
            return False

    def getUserByTg(tg_id, login):
        database = sqlite3.connect('data.db')
        cursor = database.cursor()
        cursor = cursor.execute('SELECT user_id FROM telegram WHERE tg_id = %d' % (tg_id))
        id = cursor.fetchall()
        cursor = cursor.execute('SELECT id FROM users WHERE login = "%s"' % (login))
        if cursor.fetchone() != None:
            user_id = cursor.fetchone()[0]
        else:
            return True
        if id != None:
            for i in id:
                if i[0] == user_id:
                    database.close()
                    return True
            database.close()
            return False
        else:
            database.close()
            return False

    def getUserId(login : str):
        cursor = database.cursor()
        cursor.execute("SELECT id FROM users WHERE login = '%s'" % (login))
        id = cursor.fetchone()
        if id != None:
            id = id[0]
            return id
        else:
            return False

    def getStudentsID(login : str):
        cursor = database.cursor()
        cursor.execute("SELECT id FROM users WHERE login = '%s'" % (login))
        id = cursor.fetchone()
        if id != None:
            id = id[0]
            cursor.execute("SELECT id FROM sudents WHERE user_id = '%s'" % (id))
            id = cursor.fetchall()
            return id
        else:
            logger.info("no such user")
            return ('no such user')

    def createStudent(login : str, externalID : str, name : str):
        userId = DBWork.getUserId(login)
        database.execute("INSERT INTO students (user_id, external_id, name) VALUES (%d, %d, '%s')" % (int(userId), int(externalID), name))
        logger.info("student created")
        print('student created')
        database.commit()

    # def deliteUser(login : str):
    #     database = sqlite3.connect('data.db')
    #     for i in range (0, len(DBWork.getStudentsID(login))):
    #         database.execute("DELETE FROM marks WHERE student_id = %d" % (DBWork.getStudentsID(login)[i]))
    #     database.execute("DELETE FROM students WHERE user_id = %d" % (DBWork.getUserId(login)))
    #     database.execute("DELETE FROM users WHERE login = '%s'" % login)      
    #     logger.info("user %s delitet" % (login))
    #     print('user delited')
    #     database.commit()
    #     database.close()

    def deleteStudent(login : str, externalID : str, name : str):
        userId = DBWork.getUserId(login)
        database.execute("DELETE FROM students WHERE user_id = %d" % (int(userId)))
        logger.info("student deleted")
        print('student deleted')
        database.commit()

    def checkMark(studentID, type : str, mark : str, subject : str, date):
        cursor = database.cursor()
        cursor = cursor.execute("SELECT * FROM marks WHERE student_id = %d AND type = '%s' AND mark = '%s' AND subj = '%s' AND date = '%s'" % (int(studentID), type, mark, subject, date))
        if cursor.fetchone() == None:
            print('[*]Up to date')
            return True            
        else:
            return False
    # def deliteUser(login : str):
    #     database = sqlite3.connect('data.db')
    #     for i in range (0, len(DBWork.getStudentsID(login))):
    #         database.execute("DELETE FROM marks WHERE student_id = %d" % (DBWork.getStudentsID(login)[i]))
    #     database.execute("DELETE FROM students WHERE user_id = %d" % (DBWork.getUserId(login)))
    #     database.execute("DELETE FROM users WHERE login = '%s'" % login)      
    #     logger.info("user %s delitet" % (login))
    #     print('user delited')
    #     database.commit()
    #     database.close()

    def getStudentIDByEXT(externalID):
        cursor = database.cursor()
        cursor = cursor.execute('SELECT id FROM students WHERE external_id = %s' % (externalID))
        id = cursor.fetchone()
        if id != None:
            id = id[0]
            return id
        else:
            return False

    def getTgByUser(login):
        database = sqlite3.connect('data.db')
        cursor = database.cursor()
        cursor = cursor.execute('SELECT tg_id FROM telegram WHERE user_id = %d' % (DBWork.getUserId(login)))
        id = cursor.fetchone()
        if id != None:
            id = id[0]
            database.close()
            return id
        else:
            database.close()
            return False

    def uploadMark(studentID : int, type : str, mark : str, subject : str, date):
        database.execute("INSERT INTO marks (student_id, type, mark, subj, date) VALUES (%d, '%s', '%s', '%s', '%s')" % (studentID, type, mark, subject, date))
        database.commit()

    def addUser(login : str, password : str, allStudents, tg_id):
        database = sqlite3.connect('data.db')
        database.execute("INSERT INTO users (login, pass) VALUES ('%s', '%s')" % (login, password))
        print('user created')       
        database.commit()
        for i in range (0, len(allStudents)):
            DBWork.createStudent(login, allStudents[i]['id'], allStudents[i]['name'])
            print('|__student add')      
        logger.info("students add")
        database.commit()
        database.execute("INSERT INTO telegram (user_id, tg_id) VALUES (%d, %d)" % (DBWork.getUserId(login), tg_id))
        print('telegram add')
        logger.info("telegram add")
        database.commit()
        database.close()

    def deliteUser(login : str, allStudents):
        database = sqlite3.connect('data.db')
        database.execute("DELETE FROM users WHERE login = '%s'" % (login))
        print('user deleted')       
        database.commit()
        for i in range (0, len(allStudents)):
            DBWork.deleteStudent(login, allStudents[i]['id'], allStudents[i]['name'])
            print('|__student dell')      
        logger.info("students dell")
        database.commit()
        database.execute("DELETE FROM telegram WHERE user_id = %d" % (DBWork.getUserId(login)))
        print('telegram dell')
        logger.info("telegram dell")
        database.commit()
        database.close()


    # def deliteUser(login : str):
    #     database = sqlite3.connect('data.db')
    #     for i in range (0, len(DBWork.getStudentsID(login))):
    #         database.execute("DELETE FROM marks WHERE student_id = %d" % (DBWork.getStudentsID(login)[i]))
    #     database.execute("DELETE FROM students WHERE user_id = %d" % (DBWork.getUserId(login)))
    #     database.execute("DELETE FROM users WHERE login = '%s'" % login)      
    #     logger.info("user %s delitet" % (login))
    #     print('user delited')
    #     database.commit()
    #     database.close()

    def addTgToUser(tg_id : int, login : str):
        database = sqlite3.connect('data.db')
        cursor = database.cursor()
        cursor = cursor.execute('SELECT id FROM users WHERE login = "%s"' % (str(login)))
        id = cursor.fetchone()
        if id != None:
            id = id[0]
            database.execute("INSERT INTO telegram (user_id, tg_id) VALUES (%d, %d)" % (id, tg_id))
            database.commit()
            database.close()
            return id
        else:
            database.close()
            return False

    def getStudentsByTg(tg_id : int):
        students = []
        database = sqlite3.connect('data.db')
        cursor = database.cursor()
        cursor = cursor.execute('SELECT user_id FROM telegram WHERE tg_id = %d' % (tg_id))
        id = cursor.fetchall()
        for i in id:
            cursor = cursor.execute('SELECT name FROM students WHERE user_id = %d' % (i))
            names = cursor.fetchall()
            for j in names:
                student = {}
                student['name'] = j
                student['id'] = i[0]
                students.append(student)
        database.close()
        return students


    def getCountUser(tg_id : int):
        database = sqlite3.connect('data.db')
        cursor = database.cursor()
        cursor = cursor.execute('SELECT user_id FROM telegram WHERE tg_id = %d' % (tg_id))
        id = cursor.fetchall()
        if len(id) > 1:
            database.close()
            return False
        else:
            database.close()
            return True

    def getTgByName(name):
        ids = []
        database = sqlite3.connect('data.db')
        cursor = database.cursor()
        cursor = cursor.execute('SELECT user_id FROM students WHERE name = "%s"' % (name))
        id = cursor.fetchone()[0]
        cursor = cursor.execute('SELECT tg_id FROM telegram WHERE user_id = %d' % (id))
        users = cursor.fetchall()
        for i in users:
            ids.append(i)
        database.close()
        return ids

    def getStudentByName(name):
        database = sqlite3.connect('data.db')
        cursor = database.cursor()
        cursor = cursor.execute('SELECT user_id FROM students WHERE name = "%s"' % (name))
        id = cursor.fetchone()
        if id != None:
            id = id[0]
            user = DBWork.getUser(id)
            database.close()
            return user
        else:
            database.close()
            return False

    def getNameByTg(tg_id):
        database = sqlite3.connect('data.db')
        cursor = database.cursor()
        cursor = cursor.execute('SELECT user_id FROM telegram WHERE tg_id = %d' % (tg_id))
        id = cursor.fetchone()
        cursor = cursor.execute('SELECT name FROM students WHERE user_id = %d' % (id))
        name = cursor.fetchone()[0]
        database.close()
        return name

    def getSubjList(name):
        allSubjects = []
        database = sqlite3.connect('data.db')
        cursor = database.cursor()
        cursor = cursor.execute('SELECT id FROM students WHERE name = "%s"' % (name))
        id = cursor.fetchone()
        cursor = cursor.execute('SELECT subj FROM marks WHERE student_id = %d' % (id[0]))
        subjects = cursor.fetchall()
        for i in subjects:
            if i[0] not in allSubjects:
                allSubjects.append(i[0])
        database.close()
        return allSubjects

    def getAllMarksBySubj(name, subj):
        allMarks = []
        database = sqlite3.connect('data.db')
        cursor = database.cursor()
        cursor = cursor.execute('SELECT user_id FROM students WHERE name = "%s"' % (name))
        id = cursor.fetchone()[0]
        cursor = cursor.execute('SELECT mark FROM marks WHERE (subj, student_id) = ("%s", %d)' % (subj, id))
        marks = cursor.fetchall()
        database.close()
        for i in marks:
            allMarks.append(i[0])
        return allMarks

    def getNameByLogin(login):
        database = sqlite3.connect('data.db')
        cursor = database.cursor()
        cursor = cursor.execute('SELECT id FROM users WHERE login = "%s"' % (login))
        id = cursor.fetchone()[0]
        cursor = cursor.execute('SELECT name FROM students WHERE user_id = %d' % (id))
        name = cursor.fetchone()[0]
        database.close()
        return name

if __name__ == '__main__':
    print(DBWork.getStudentIDByEXT(6017297))